#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    char hash[HASH_LEN];
    char password[PASS_LEN];
};

int file_length(char *filename)
{
    struct stat info;
    int ret = stat(filename, &info);
    if (ret == -1) return -1;
    else return info.st_size;
}

// TODO
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    *size = 0;
    
    int len = file_length(filename);
    char *dict = malloc(len);
    
    // Read file into memory
    FILE *f = fopen(filename, "r");
    if (!f)
    {
        printf("Can't open %s for reading\n", filename);
        exit(1);
    }
    fread(dict, sizeof(char), len, f);
    fclose(f);
    
    // Change newlines into nulls and count them as they go
    int num = 0;
    
    for (int i = 0; i < len; i++)
    {
        if (dict[i] == '\n')
        {
            dict[i] = '\0';
            num++;
        }
    }
    
    // Build struct entry
    struct entry *dictionary = malloc(num * sizeof(struct entry));
    sscanf(&dict[0], "%s", dictionary[0].password);
    char *hash = md5(&dict[0], strlen(&dict[0]));
    strcpy(dictionary[0].hash, hash);
    
    int j = 0;
    
    for (int i = 0; i < len - 1; i++)
    {
        if (dict[i] == '\0')
        {
            sscanf(&dict[i+1], "%s", dictionary[j].password);
            hash = md5(&dict[i+1], strlen(&dict[i+1]));
            strcpy(dictionary[j].hash, hash);
            j++;
        }
    }
    
    /* Test that data loaded correctly
    for (int i = 0; i < j; i++)
    {
        printf("%s %s\n", dictionary[i].hash, dictionary[i].password);
    }
    */
    
    *size = j;
    
    return dictionary;
}

int comparebyhash(const void *a, const void *b)
{
    return strcmp(((struct entry *)a)->hash, ((struct entry *)b)->hash);
}

int hashsearch(const void *target, const void *elem)
{
    return strcmp((char *)target, (*(struct entry *)elem).hash);
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the dictionary file into an array of entry structures
    int len;
    struct entry *dict = read_dictionary(argv[2], &len);
    
    /*
    printf("Unsorted:\n");
    for (int i = 0; i < len; i++)
    {
        printf("%d : %s %s\n", i, dict[i].hash, dict[i].password);
    }
    */
    
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    //qsort(dict, 0, 0, NULL);
    qsort(dict, len, sizeof(struct entry), comparebyhash);
    
    /*
    printf("Sorted:\n");
    for (int i = 0; i < len; i++)
    {
        printf("%d : %s %s\n", i, dict[i].hash, dict[i].password);
    }
    */
    
    // TODO
    // Open the hash file for reading.
    FILE *f = fopen(argv[1], "r");
    if (!f)
    {
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)
    char hash[HASH_LEN];
    while (fgets(hash, HASH_LEN, f) != NULL)
    {
        struct entry *found = bsearch(hash, dict, len, sizeof(struct entry), hashsearch);
        if (found != NULL)
        {
            printf("Found %s %s\n", found->hash, found->password);
        }
    }
}
